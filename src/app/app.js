

var app = angular.module('ngBoilerplate', [
    'templates-app',
    'templates-common',
    'ui.bootstrap',
    'ui.router',
    'placeholders',
    //'ngMessages',
    'angular-md5',
    'ncy-angular-breadcrumb',
    'ipCookie',
    'ui-notification',

    //sub modules:
    'ngBoilerplate.home',
    'ngBoilerplate.header',
    'ngBoilerplate.login',
    'ngBoilerplate.pref',
    'ngBoilerplate.high',
    'ngBoilerplate.contact',
    'ngBoilerplate.services',

    'ngBoilerplate.ui',
    'ngBoilerplate.nesting'
])

    .config(function myAppConfig($stateProvider, $urlRouterProvider) {
            'use strict';

        $urlRouterProvider
            //.when('/c?id', '/contacts/:id')
            .otherwise('/home');

        $stateProvider.state('root', {
            // url: '/default', - no url needed at all, but could be
            abstract: true,
            //url: '/home',
            views:{
                'header@root': {
                    controller: 'HeaderCtrl as Header',
                    templateUrl: 'header/header.tpl.html'
                },
                'main@root': {
                    controller: 'HomeCtrl as Home',
                    templateUrl: 'home/home.tpl.html'
                },
                'footer@root': {
                    templateUrl: 'footer/footer.tpl.html'
                }
            },
            data: {pageTitle: 'home'}
        });
    })

    .run(function run($rootScope, $state, $stateParams, Notification, ipCookie) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.config = { BACKEND_URL : 'http://bookkeeper.w/app_dev.php'};
        //$rootScope.isSidebarExpanded;
        //$rootScope.isSidebarExpanded = true;
        if (ipCookie('cookieShow')!==true) {
            ipCookie('cookieShow', true);
            Notification.info({message: 'This site is using cookies', delay: 5000});
        }
    })

    .controller('AppCtrl', function AppCtrl($scope, $location, $log) {

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle +  '| Plate ';
            }
        });

            //$scope.foo = 'bar';

        //$log.log('Hello log');
        //$log.info('AppCtrl info');
        //$log.warn('Hello warn');
        //$log.debug('Hello debug');
        //$log.error('Hello error');
    })


;

