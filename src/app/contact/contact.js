
angular.module('ngBoilerplate.contact', [
    //'ngResource',
    //'restangular'
])
    .config(function config($stateProvider) {
        $stateProvider.state('contact', {
            parent: 'root',
            url: '/contact',
            views: {
                "main": {
                    controller: 'ContactCtrl',
                    templateUrl: 'contact/contact.tpl.html'
                }
            },
            data: {pageTitle: 'contact'},
            ncyBreadcrumb: {
                label: 'Contact'
            }
        });
    })
    .controller('FormCtrl', function ($scope, $http, $filter, $log, md5, Notification) {

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-event');
            if ($scope.employeeForm.$invalid) {
                return;
            }

            if ($scope.editableEmployee.id === 0) {
                // insert new employee
                DataService.insertEmployee($scope.editableEmployee);
            } else {
                // update the employee
                DataService.updateEmployee($scope.editableEmployee);
            }

            $scope.employee = angular.copy($scope.editableEmployee);
            $window.history.back();
        };
        $scope.cancelForm = function () {
            $window.history.back();
        };
        $scope.resetForm = function () {
            $scope.$broadcast('hide-errors-event');
        };


        $scope.readRest = function () {
            //Notification.info({message: 'REST', delay: 5000});
            $http.get($scope.config.BACKEND_URL + '/api').success(function (data, status, headers) {
                $log.log(data);
            })
                .success(function (data, status, headers) {
                    if(data.success === true){

                    } else {
                        Notification.error({ message: data.message, delay: 5000});
                    }
                })
                .error(function (data, status, headers) {
                    Notification.error({message: 'Error ' + data.error.code + '. ' + data.error.message, delay: 5000});
                });
        };

        $scope.read = function () {
            $http.get($scope.config.BACKEND_URL + '/book/books/').success(function (data, status, headers) {
                $scope.books = data;
            });
            //$log.log('POST: form | first:' + $scope.person.first + ' Last:'+$scope.person.last );
            //$http.put('http://book.cc/hi', $scope.person);
        };
        $scope.read();

        var defaultForm = {
            title: "",
            pages: "",
            description: ""
        };
        $scope.find = function (store, index) {
            //var found = $filter('filter')($scope.books, {index: index}, true);
            var found = $filter('filter')(store, {index: index}, true);
            if (found.length) {
                //$log.log('here: '+JSON.stringify(found[0]));
                return found[0];
            }
        };

        $scope.save = function (newBook) {
            if(newBook === defaultForm){
                return;
            }
            newBook.index = md5.createHash(newBook.title + Math.floor((Math.random() * 100) + 1));
            newBook.buttonRemoveDisabled = true;
            $scope.books.push(newBook);
            $http.post($scope.config.BACKEND_URL + '/book/books/create', newBook)
                .success(function (data, status, headers, config) {
                    var record = $scope.find($scope.books, data.data.index);
                    if (data.success === true) {
                        if (record) {
                            record.id = data.data.id;
                            record.buttonRemoveDisabled = false;
                        }
                        //$scope.read();
                    } else {
                        if (record) {
                            $scope.books = $filter('filter')($scope.books, {index: '!' + data.data.index});
                            //$scope.books.splice( $scope.books.indexOf(record), 1 );
                        } else {
                            $scope.books.pop();
                        }
                    }
                })
                .error(function (data, status, headers) {
                    $scope.books.pop();
                })
            ;
            $scope.newBook = angular.copy(defaultForm);
        };

        $scope.remove = function (index, $id, disable ) {
            disable = typeof disable !== 'undefined' ? disable : false;
            //if(disable === true){
            //    console.info('1 disable:'+disable);
            //} else {
            //    console.info('0 disable:'+disable);
            //}
            $scope.books.splice(index, 1);
            $http['delete']($scope.config.BACKEND_URL + '/book/books/delete/' + $id)
                .success(function (data, status, headers, config) {
                    if (data.success === false) {
                        Notification.error({message: 'Delete: success: success: false', delay: 5000});
                        //Notification.error({message: data.message, delay: 5000});
                    }
                })
                .error(function (data, status, headers) {
                    Notification.error({message: 'Delete: error', delay: 5000});
                    //Notification.error({message: data.message, delay: 5000});
                });
        };
        $scope.users = [
            {name: "Moroni", age: 50},
            {name: "Tiancum", age: 43},
            {name: "Jacob", age: 27}
        ];
    })

    .controller('ContactExtraCtrl', function ($scope, $window, $http, $log) {
        //$http.get('http://bookkeeper.w/app_dev.php/hi')
        $http.get($scope.config.BACKEND_URL + '/hi')
            .success(function (data, status, headers, config) {
                //data.success = 'data.simple';
                //$log.log('request succeded');

                $scope.extra = {
                    world: 'Jupiter(2)',
                    data: data,
                    additional: 'additional_value',
                    simple: 'simple Success',
                    backendUrl: $scope.config.BACKEND_URL
                };

                //var world = data.world;
                //$scope.extra.world = world;

                //var data_deserialized = JSON.parse( data );
                //$scope.extra.headers = data_deserialized.world;
            })
            .error(function (data, status, headers, config) {
                $log.warn('request failed');

                $scope.extra = {
                    data: 'no data',
                    simple: 'simple Failure',
                    backendUrl: $scope.config.BACKEND_URL
                };
            });
    })

    .controller('ContactCtrl', function ($scope, $window, $http) {
        $http.get('http://rest-service.guides.spring.io/greeting')
            .success(function (data) {
                $scope.greeting = data;
            });
    })

;

angular.module('ngBoilerplate.services', [
    'restangular'
])
    //.factory('Entry', function ($resource) {
    //    return $resource('/api/entries/:id'); // Note the full endpoint address
    //})
    //
    //.controller('ResourceController', function ($scope, Entry) {
    //    var entry = Entry.get({id: $scope.id}, function () {
    //        console.log(entry);
    //    }); // get() returns a single entry
    //
    //    var entries = Entry.query(function () {
    //        console.log(entries);
    //    }); //query() returns all the entries
    //
    //    $scope.entry = new Entry(); //You can instantiate resource class
    //
    //    $scope.entry.data = 'some data';
    //
    //    Entry.save($scope.entry, function () {
    //        //data saved. do something here.
    //    }); //saves an entry. Assuming $scope.entry is the Entry object
    //})


    // Using RESTangular
    .controller('restCtrl', function ($scope, Restangular) {

        // Only stating main route
        Restangular.all('accounts');

        // Stating main object
        Restangular.one('accounts', 1234);

        // Gets a list of all of those accounts
        Restangular.several('accounts', 1234, 123, 12345);


        // First way of creating a Restangular object. Just saying the base URL
        var baseAccounts = Restangular.all('accounts');

        // This will query /accounts and return a promise.
        baseAccounts.getList().then(function (accounts) {
            $scope.allAccounts = accounts;
        });

        // Does a GET to /accounts
        // Returns an empty array by default. Once a value is returned from the server
        // that array is filled with those values. So you can use this in your template
        $scope.accounts = Restangular.all('accounts').getList().$object;

        var newAccount = {name: "Gonto's account"};

        // POST /accounts
        baseAccounts.post(newAccount);

        // GET to http://www.google.com/ You set the URL in this case
        Restangular.allUrl('googlers', 'http://www.google.com/').getList();

        // GET to http://www.google.com/1 You set the URL in this case
        Restangular.oneUrl('googlers', 'http://www.google.com/1').get();

        // You can do RequestLess "connections" if you need as well

        // Just ONE GET to /accounts/123/buildings/456
        Restangular.one('accounts', 123).one('buildings', 456).get();

        // Just ONE GET to /accounts/123/buildings
        Restangular.one('accounts', 123).getList('buildings');

        // Here we use Promises then
        // GET /accounts
        baseAccounts.getList().then(function (accounts) {
            // Here we can continue fetching the tree :).

            var firstAccount = accounts[0];
            // This will query /accounts/123/buildings considering 123 is the id of the firstAccount
            $scope.buildings = firstAccount.getList("buildings");

            // GET /accounts/123/places?query=param with request header: x-user:mgonto
            $scope.loggedInPlaces = firstAccount.getList("places", {query: param}, {'x-user': 'mgonto'});

            // This is a regular JS object, we can change anything we want :)
            firstAccount.name = "Gonto";

            // If we wanted to keep the original as it is, we can copy it to a new element
            var editFirstAccount = Restangular.copy(firstAccount);
            editFirstAccount.name = "New Name";


            // PUT /accounts/123. The name of this account will be changed from now on
            firstAccount.put();
            editFirstAccount.put();

            // PUT /accounts/123. Save will do POST or PUT accordingly
            firstAccount.save();

            // DELETE /accounts/123 We don't have first account anymore :(
            firstAccount.remove();

            var myBuilding = {
                name: "Gonto's Building",
                place: "Argentina"
            };

            // POST /accounts/123/buildings with MyBuilding information
            firstAccount.post("Buildings", myBuilding).then(function () {
                console.log("Object saved OK");
            }, function () {
                console.log("There was an error saving");
            });

            // GET /accounts/123/users?query=params
            firstAccount.getList("users", {query: params}).then(function (users) {
                // Instead of posting nested element, a collection can post to itself
                // POST /accounts/123/users
                users.post({userName: 'unknown'});

                // Custom methods are available now :).
                // GET /accounts/123/users/messages?param=myParam
                users.customGET("messages", {param: "myParam"});

                var firstUser = users[0];

                // GET /accounts/123/users/456. Just in case we want to update one user :)
                $scope.userFromServer = firstUser.get();

                // ALL http methods are available :)
                // HEAD /accounts/123/users/456
                firstUser.head();

            });

        }, function errorCallback() {
            alert("Oops error from server :(");
        });

        // Second way of creating Restangular object. URL and ID :)
        var account = Restangular.one("accounts", 123);

        // GET /accounts/123?single=true
        $scope.account = account.get({single: true});

        // POST /accounts/123/messages?param=myParam with the body of name: "My Message"
        account.customPOST({name: "My Message"}, "messages", {param: "myParam"}, {});


    })

;