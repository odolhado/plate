
angular.module('ngBoilerplate.header', [])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
    .config(function config($stateProvider) {
        //$stateProvider.state('header', {
        //    parent: 'root',
        //    url: '/header
        //    views: {
        //        "header": {
        //            controller: 'HeaderCtrl',
        //            templateUrl: 'header/header.tpl.html'
        //        }
        //    }
        //});
    })

/**
 * And of course we define a controller for our route.
 */
    .controller('HeaderCtrl', function HomeController($scope, $log) {

        //$log.info('HeaderCtrl info');

    })



;

