angular.module('ngBoilerplate.high', [

])

    .config(function config($stateProvider) {
        $stateProvider.state('high', {
            parent: 'root',
            url: '/high',
            views: {
                "main": {
                    controller: 'HighCtrl',
                    templateUrl: 'high/high.tpl.html',
                    class: 'container'
                }
            },
            data: {pageTitle: 'highcharts'},
            ncyBreadcrumb: {
                label: 'Highcharts'
            }
        });
    })

    .controller('HighCtrl', function HighCtrl($scope) {

    })

    .controller('DashboardCtrl', ['$scope', '$http', 'chart', function DashboardController($scope, $http, chart) {
        $scope.model = {
            periods: [
                {key: 'daily', value: 'Daily'},
                {key: 'weekly', value: 'Weekly'},
                {key: 'monthly', value: 'Monthly'}
            ],
            choice: 'daily'
        };

        $scope.changeChart = function () {
            $http.get(chart.getUrlForChoice($scope.model.choice))
                .success(function (data) {
                    var options = chart.getOptionsFromData(data, $scope.model.choice);
                    chart.draw(options);
                });
        };

        $scope.changeChart();
    }])
    .factory('chart', ['configuration', function (configuration) {
        var factory = {};

        factory.getUrlForChoice = function (choice) {
            return configuration.resource.statistics + choice;
        };

        factory.getOptionsFromData = function (data, choice) {
            var avg = [], current = [], categories = [];

            angular.forEach(data.reverse(), function (statistics) {
                var avgValue = parseFloat(statistics.avg - statistics.value).toFixed(4);
                avg.push(parseFloat(avgValue));
                current.push(statistics.value);

                if ('weekly' === choice) {
                    categories.push(statistics.created_to);
                } else {
                    categories.push(statistics.created_at);
                }
            });

            var options = {};
            options.series = [{
                name: 'Left to avg',
                data: avg
            }, {
                name: 'Current',
                data: current
            }];

            options.xAxis = {
                categories: categories
            };

            return options;
        };

        factory.getDefaults = function () {
            return {
                colors: ['#8cffca', '#ff8d7e'],
                chart: {
                    renderTo: 'chartContainer',
                    type: 'column'
                },
                title: {
                    text: 'Average usage bind with current usage'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Average'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' +
                            'Total: ' + this.point.stackTotal;
                    }
                },
                plotOptions: {
                    series: {
                        pointPadding: 0.0001, groupPadding: 0.01
                    },
                    column: {
                        animation: {
                            duration: 1000
                        },
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black'
                            }
                        }
                    }
                }
            };
        };
        factory.draw = function (options) {
            var defaults = factory.getDefaults();
            options = angular.extend({}, defaults, options);
            new Highcharts.Chart(options);
        };
        return factory;
    }])

    .controller('chartController', ['$scope', function ($scope) {

        var chart = new Highcharts.Chart({
            colors: ['#8cffca', '#ff8d7e'],
            chart: {
                renderTo: 'chartContainer',
                type: 'column'
            },
            title: {
                text: 'Average usage bind with current usage'
            },
            xAxis: {
                categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Average'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },

            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal;
                }
            },

            plotOptions: {
                series: {
                    pointPadding: 0.0001, groupPadding: 0.01
                },
                column: {
                    animation: {
                        duration: 1000
                    },
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black'
                        }
                    }
                }
            },

            series: [{
                name: 'Left to avg',
                data: [2500, 3200, 2400, 2086, 3141, 2785, 2605, 2897, 3000, 2995, 3600, 2245]
            }, {
                name: 'Current',
                data: [2491, 1562, 2789, 1000, 1943, 1789, 1830, 2500, 2257, 2435, 1594, 1733]
            }]

        });

    }])

    .controller('chart2Controller', ['$scope', function ($scope) {

        var data = ("# ----------------------------------------\n# highcharts.com\n# Audience Overview\n# 20130309-20130408\n# ----------------------------------------\nDay,Visits,Unique Visitors\n3/9/13,5691,4346\n3/10/13,5403,4112\n3/11/13,15574,11356\n3/12/13,16211,11876\n3/13/13,16427,11966\n3/14/13,16486,12086\n3/15/13,14737,10916\n3/16/13,5838,4507\n3/17/13,5542,4202\n3/18/13,15560,11523\n3/19/13,18940,14431\n3/20/13,16970,12599\n3/21/13,17580,13094\n3/22/13,17511,13234\n3/23/13,6601,5213\n3/24/13,6158,4806\n3/25/13,17353,12639\n3/26/13,17660,12768\n3/27/13,16921,12389\n3/28/13,15964,11686\n3/29/13,12028,8891\n3/30/13,5835,4513\n3/31/13,4799,3661\n4/1/13,13037,9503\n4/2/13,16976,12666\n4/3/13,17100,12635\n4/4/13,15701,11394\n4/5/13,14378,10530\n4/6/13,5889,4521\n4/7/13,6779,5109\n4/8/13,16068,11599\n");
        //colors: ['#8cffca', '#ff8d7e'],
        //chart: {
        //    renderTo: 'chart2Container',
        //    type: 'column'
        //},

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'chart2Container'
            },
            data: {
                csv: data
            },

            title: {
                text: 'Title of the chart'
            },

            subtitle: {
                text: 'Source: Subtitle of the chart'
            },

            xAxis: {
                tickInterval: 7 * 24 * 3600 * 1000, // one week
                tickWidth: 0,
                gridLineWidth: 1,
                labels: {
                    align: 'left',
                    x: 3,
                    y: -3
                }
            },

            yAxis: [{ // left y axis
                title: {
                    text: null
                },
                labels: {
                    align: 'left',
                    x: 3,
                    y: 16,
                    format: '{value:.,0f}'
                },
                showFirstLabel: false
            }, { // right y axis
                linkedTo: 0,
                gridLineWidth: 0,
                opposite: true,
                title: {
                    text: null
                },
                labels: {
                    align: 'right',
                    x: -3,
                    y: 16,
                    format: '{value:.,0f}'
                },
                showFirstLabel: false
            }],

            legend: {
                align: 'left',
                verticalAlign: 'top',
                y: 20,
                floating: true,
                borderWidth: 0
            },

            tooltip: {
                shared: true,
                crosshairs: true
            },

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: e.pageX || e.clientX,
                                        y: e.pageY || e.clientY
                                    },
                                    headingText: this.series.name,
                                    maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' +
                                    this.y + ' visits',
                                    width: 200
                                });
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            },

            series: [{
                name: 'All visits',
                lineWidth: 4,
                marker: {
                    radius: 4
                }
            }, {
                name: 'New visitors'
            }]
        });

    }])


    .controller('chart3Controller', ['$scope', function ($scope) {

        var data = ("# ----------------------------------------\n# highcharts.com\n# Audience Overview\n# 20130309-20130408\n# ----------------------------------------\nDay,Visits,Unique Visitors\n3/9/13,5691,4346\n3/10/13,5403,4112\n3/11/13,15574,11356\n3/12/13,16211,11876\n3/13/13,16427,11966\n3/14/13,16486,12086\n3/15/13,14737,10916\n3/16/13,5838,4507\n3/17/13,5542,4202\n3/18/13,15560,11523\n3/19/13,18940,14431\n3/20/13,16970,12599\n3/21/13,17580,13094\n3/22/13,17511,13234\n3/23/13,6601,5213\n3/24/13,6158,4806\n3/25/13,17353,12639\n3/26/13,17660,12768\n3/27/13,16921,12389\n3/28/13,15964,11686\n3/29/13,12028,8891\n3/30/13,5835,4513\n3/31/13,4799,3661\n4/1/13,13037,9503\n4/2/13,16976,12666\n4/3/13,17100,12635\n4/4/13,15701,11394\n4/5/13,14378,10530\n4/6/13,5889,4521\n4/7/13,6779,5109\n4/8/13,16068,11599\n");

        var chart = new Highcharts.Chart({
            colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
                "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
            chart: {
                renderTo: 'chart3Container',
                type: 'spline',
                backgroundColor: {
                    linearGradient: {x1: 0, y1: 0, x2: 1, y2: 1},
                    stops: [
                        [0, '#2a2a2b'],
                        [1, '#3e3e40']
                    ]
                }
            },
            title: {
                text: 'Snow depth at Vikjafjellet, Norway'
            },
            subtitle: {
                text: 'Irregular time data in Highcharts JS'
            },
            legend: {
                itemStyle: {
                    color: '#E0E0E3'
                },
                itemHoverStyle: {
                    color: '#FFF'
                },
                itemHiddenStyle: {
                    color: '#606063'
                }
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [{
                name: 'Winter 2007-2008',
                // Define the data points. All series have a dummy year
                // of 1970/71 in order to be compared on the same x axis. Note
                // that in JavaScript, months start at 0 for January, 1 for February etc.
                data: [
                    [Date.UTC(1970, 9, 27), 0],
                    [Date.UTC(1970, 10, 10), 0.6],
                    [Date.UTC(1970, 10, 18), 0.7],
                    [Date.UTC(1970, 11, 2), 0.8],
                    [Date.UTC(1970, 11, 9), 0.6],
                    [Date.UTC(1970, 11, 16), 0.6],
                    [Date.UTC(1970, 11, 28), 0.67],
                    [Date.UTC(1971, 0, 1), 0.81],
                    [Date.UTC(1971, 0, 8), 0.78],
                    [Date.UTC(1971, 0, 12), 0.98],
                    [Date.UTC(1971, 0, 27), 1.84],
                    [Date.UTC(1971, 1, 10), 1.80],
                    [Date.UTC(1971, 1, 18), 1.80],
                    [Date.UTC(1971, 1, 24), 1.92],
                    [Date.UTC(1971, 2, 4), 2.49],
                    [Date.UTC(1971, 2, 11), 2.79],
                    [Date.UTC(1971, 2, 15), 2.73],
                    [Date.UTC(1971, 2, 25), 2.61],
                    [Date.UTC(1971, 3, 2), 2.76],
                    [Date.UTC(1971, 3, 6), 2.82],
                    [Date.UTC(1971, 3, 13), 2.8],
                    [Date.UTC(1971, 4, 3), 2.1],
                    [Date.UTC(1971, 4, 26), 1.1],
                    [Date.UTC(1971, 5, 9), 0.25],
                    [Date.UTC(1971, 5, 12), 0]
                ]
            }, {
                name: 'Winter 2008-2009',
                data: [
                    [Date.UTC(1970, 9, 18), 0],
                    [Date.UTC(1970, 9, 26), 0.2],
                    [Date.UTC(1970, 11, 1), 0.47],
                    [Date.UTC(1970, 11, 11), 0.55],
                    [Date.UTC(1970, 11, 25), 1.38],
                    [Date.UTC(1971, 0, 8), 1.38],
                    [Date.UTC(1971, 0, 15), 1.38],
                    [Date.UTC(1971, 1, 1), 1.38],
                    [Date.UTC(1971, 1, 8), 1.48],
                    [Date.UTC(1971, 1, 21), 1.5],
                    [Date.UTC(1971, 2, 12), 1.89],
                    [Date.UTC(1971, 2, 25), 2.0],
                    [Date.UTC(1971, 3, 4), 1.94],
                    [Date.UTC(1971, 3, 9), 1.91],
                    [Date.UTC(1971, 3, 13), 1.75],
                    [Date.UTC(1971, 3, 19), 1.6],
                    [Date.UTC(1971, 4, 25), 0.6],
                    [Date.UTC(1971, 4, 31), 0.35],
                    [Date.UTC(1971, 5, 7), 0]
                ]
            }, {
                name: 'Winter 2009-2010',
                data: [
                    [Date.UTC(1970, 9, 9), 0],
                    [Date.UTC(1970, 9, 14), 0.15],
                    [Date.UTC(1970, 10, 28), 0.35],
                    [Date.UTC(1970, 11, 12), 0.46],
                    [Date.UTC(1971, 0, 1), 0.59],
                    [Date.UTC(1971, 0, 24), 0.58],
                    [Date.UTC(1971, 1, 1), 0.62],
                    [Date.UTC(1971, 1, 7), 0.65],
                    [Date.UTC(1971, 1, 23), 0.77],
                    [Date.UTC(1971, 2, 8), 0.77],
                    [Date.UTC(1971, 2, 14), 0.79],
                    [Date.UTC(1971, 2, 24), 0.86],
                    [Date.UTC(1971, 3, 4), 0.8],
                    [Date.UTC(1971, 3, 18), 0.94],
                    [Date.UTC(1971, 3, 24), 0.9],
                    [Date.UTC(1971, 4, 16), 0.39],
                    [Date.UTC(1971, 4, 21), 0]
                ]
            }]
        });
    }])
    .controller('chart4Controller', ['$scope', function ($scope) {

        var data = ("# ----------------------------------------\n# highcharts.com\n# Audience Overview\n# 20130309-20130408\n# ----------------------------------------\nDay,Visits,Unique Visitors\n3/9/13,5691,4346\n3/10/13,5403,4112\n3/11/13,15574,11356\n3/12/13,16211,11876\n3/13/13,16427,11966\n3/14/13,16486,12086\n3/15/13,14737,10916\n3/16/13,5838,4507\n3/17/13,5542,4202\n3/18/13,15560,11523\n3/19/13,18940,14431\n3/20/13,16970,12599\n3/21/13,17580,13094\n3/22/13,17511,13234\n3/23/13,6601,5213\n3/24/13,6158,4806\n3/25/13,17353,12639\n3/26/13,17660,12768\n3/27/13,16921,12389\n3/28/13,15964,11686\n3/29/13,12028,8891\n3/30/13,5835,4513\n3/31/13,4799,3661\n4/1/13,13037,9503\n4/2/13,16976,12666\n4/3/13,17100,12635\n4/4/13,15701,11394\n4/5/13,14378,10530\n4/6/13,5889,4521\n4/7/13,6779,5109\n4/8/13,16068,11599\n");

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'chart4Container',
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                },
                backgroundColor: {
                    linearGradient: {x1: 0, y1: 0, x2: 1, y2: 1},
                    stops: [
                        [0, '#2a2a2b'],
                        [1, '#3e3e40']
                    ]
                },
                colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
                    "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"]
            },
            title: {
                text: 'Live random data'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Random data',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -49; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                }())
            }]
        });
    }])


;
