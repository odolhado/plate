angular.module('ngBoilerplate.login', [])

    .config(function config($stateProvider) {
        $stateProvider.state('login', {
            parent: 'root',
            url: '/login',
            views: {
                "main@root": {
                    controller: 'LoginCtrl',
                    templateUrl: 'login/login.tpl.html'
                }
            },
            data: {pageTitle: 'login'},
            ncyBreadcrumb: {
                label: 'Login'
            }
        })
        ;

    })

    .controller('LoginCtrl', function LoginCtrl($scope, $log) {
        $log.info('LoginCtrl info');
    })

    .directive('employeeForm', function () {
        return {
            restrict: 'E',
            templateUrl: 'login/efTemplate.tpl.html'
        };
    })


    //.controller('efController', function efController($scope, $window, $routeParams, DataService) {
    .controller('efController', function efController($scope, $window, DataService) {

        //if ($routeParams.id)
        //    $scope.employee = DataService.getEmployee($routeParams.id);
        //else
        //    $scope.employee = { id: 0 };

        $scope.editableEmployee = angular.copy($scope.employee);

        $scope.departments = [
            "Engineering",
            "Marketing",
            "Finance",
            "Administration"
        ];

        $scope.shouldShowFullName = function () {
            return true;
        };

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-event');

            if ($scope.employeeForm.$invalid) {
                return;
            }

            if ($scope.editableEmployee.id === 0) {
                // insert new employee
                DataService.insertEmployee($scope.editableEmployee);
            }
            else {
                // update the employee
                DataService.updateEmployee($scope.editableEmployee);
            }

            $scope.employee = angular.copy($scope.editableEmployee);
            $window.history.back();
        };

        $scope.cancelForm = function () {
            $window.history.back();
        };

        $scope.resetForm = function () {
            $scope.$broadcast('hide-errors-event');
        };

    })

;
