angular.module('ngBoilerplate.nesting', [
    'ui.router'
])
    .config(function config($urlRouterProvider, $stateProvider) {
        $stateProvider
            .state('nesting', {
                parent: 'root',
                url: '/nesting',
                views: {
                    "main": {
                        controller: 'NestCtrl',
                        templateUrl: 'nesting/nesting.tpl.html'
                    }
                },
                data: {pageTitle: 'nest'},
                ncyBreadcrumb: {
                    label: 'Root'
                }
            })
            .state('nesting.child', {
                parent: 'nesting',
                url: '/child',
                templateUrl: 'nesting/nesting2.tpl.html',
                controller: function ($scope, $state) {
                    $scope.state = $state.current;
                },
                data: {pageTitle: 'child'},
                ncyBreadcrumb: {
                    label: 'Child'
                }
            })
            .state('nesting.child.child', {
                parent: 'nesting.child',
                url: '/child',
                templateUrl: 'nesting/nesting3.tpl.html',
                controller: function ($scope, $state) {
                    $scope.state = $state.current;
                },
                data: {pageTitle: 'second child'},
                ncyBreadcrumb: {
                    label: 'Second child'
                }
            })
            .state('nesting.child.child.child', {
                parent: 'nesting.child.child',
                url: '/child',
                templateUrl: 'nesting/nesting4.tpl.html',
                controller: function ($scope, $state) {
                    $scope.state = $state.current;
                },
                data: {pageTitle: 'third child'},
                ncyBreadcrumb: {
                    label: 'Third child'
                }
            })
            ;
    })
    .controller('NestCtrl', function ($scope) {
        //$scope.state = $state.current;
    })

;