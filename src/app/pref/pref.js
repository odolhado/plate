angular.module('ngBoilerplate.pref', [])

    .config(function config($stateProvider) {
        $stateProvider.state('pref', {
            parent: 'root',
            url: '/pref',
            views: {
                "main": {
                    controller: 'PrefCtrl',
                    templateUrl: 'pref/pref.tpl.html'
                }
            },
            data: {pageTitle: 'preferences'},
            ncyBreadcrumb: {
                label: 'Preferences'
            }
        });
    })

    .controller('PrefCtrl', function PrefCtrl($scope) {

    })

;
