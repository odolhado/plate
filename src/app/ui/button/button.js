angular.module('ngBoilerplate.button', [])

    .config(function config($stateProvider) {
        $stateProvider.state('ui.button', {
            parent: 'ui',
            url: '/button',
            templateUrl: 'ui/button/button.tpl.html',
            data: {pageTitle: 'buttons'},
            ncyBreadcrumb: {
                label: 'Buttons'
            }
        });
    })

    // ACCORDION
    .controller('AccordionDemoCtrl', function ($scope) {
        $scope.oneAtATime = true;

        $scope.groups = [
            {
                title: 'Dynamic Group Header - 1',
                content: 'Dynamic Group Body - 1'
            },
            {
                title: 'Dynamic Group Header - 2',
                content: 'Dynamic Group Body - 2'
            }
        ];

        $scope.items = ['Item 1', 'Item 2', 'Item 3'];

        $scope.addItem = function () {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
        };

        $scope.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };
    })

    // ALERT
    .controller('AlertDemoCtrl', function ($scope) {
        $scope.alerts = [
            {type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.'},
            {type: 'success', msg: 'Well done! You successfully read this important alert message.'}
        ];

        $scope.addAlert = function () {
            $scope.alerts.push({msg: 'Another alert!'});
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
    })

    // BUTTONS
    .controller('ButtonsCtrl', function ($scope) {
        $scope.singleModel = 1;

        $scope.radioModel = 'Middle';

        $scope.checkModel = {
            left: false,
            middle: true,
            right: false
        };
    })

    // Carousel
    .controller('CarouselDemoCtrl', function ($scope) {
        $scope.myInterval = 5000;
        var slides = $scope.slides = [];
        $scope.addSlide = function () {
            var newWidth = 600 + slides.length + 1;
            slides.push({
                image: 'http://placekitten.com/' + newWidth + '/300',
                text: ['More', 'Extra', 'Lots of', 'Surplus'][slides.length % 4] + ' ' +
                ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
            });
        };
        for (var i = 0; i < 4; i++) {
            $scope.addSlide();
        }
    })

    //Collapsed
    .controller('CollapseDemoCtrl', function ($scope) {
        $scope.isCollapsed = false;
    })


// Dropdown : ui.bootstrap.dropdown  ! dropdownToggle
    .controller('DropdownCtrl', function ($scope, $log) {
        $scope.items = [
            'The first choice!',
            'And another choice for you.',
            'but wait! A third!'
        ];

        $scope.status = {
            isopen: false
        };

        $scope.toggled = function (open) {
            $log.log('Dropdown is now: ', open);
        };

        $scope.toggleDropdown = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };
    })

    // Progress Bars
    .controller('ProgressDemoCtrl', function ($scope) {
        $scope.max = 200;

        $scope.random = function () {
            var value = Math.floor((Math.random() * 100) + 1);
            var type;

            if (value < 25) {
                type = 'success';
            } else if (value < 50) {
                type = 'info';
            } else if (value < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }

            $scope.showWarning = (type === 'danger' || type === 'warning');

            $scope.dynamic = value;
            $scope.type = type;
        };
        $scope.random();

        $scope.randomStacked = function () {
            $scope.stacked = [];
            var types = ['success', 'info', 'warning', 'danger'];

            for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
                var index = Math.floor((Math.random() * 4));
                $scope.stacked.push({
                    value: Math.floor((Math.random() * 30) + 1),
                    type: types[index]
                });
            }
        };
        $scope.randomStacked();
    })

    //Rating bars
    .controller('RatingDemoCtrl', function ($scope) {
        $scope.rate = 7;
        $scope.max = 10;
        $scope.isReadonly = false;

        $scope.hoveringOver = function (value) {
            $scope.overStar = value;
            $scope.percent = 100 * (value / $scope.max);
        };

        $scope.ratingStates = [
            {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
            {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
            {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
            {stateOn: 'glyphicon-heart'},
            {stateOff: 'glyphicon-off'}
        ];
    })

//  Modal
    .controller('ModalDemoCtrl', function ($scope, $modal, $log) {

        $scope.items = ['item1', 'item2', 'item3'];

        $scope.open = function (size) {

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    })
    .controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

        $scope.items = items;
        $scope.selected = {
            item: $scope.items[0]
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cance');
        };
    })

    //Nested menu(3):
    .controller('NestedMenuCtrl', function ($scope) {
        $scope.menu = [
            {
                "title": "Home",
                "href": "#"
            },
            {
                "title": "About",
                "href": "about"
            },
            {
                "title": "History",
                "href": "about/history"
            },
            {
                "title": "Contact",
                "href": "contact"
            },
            {
                "title": "Other things - in a list. (Click here)",
                "submenu": [
                    {
                        "header": "Sample Header"
                    },
                    {
                        "title": "Some Link",
                        "href": "some/place"
                    },
                    {
                        "title": "Another Link",
                        "href": "some/other/place"
                    },
                    {
                        "divider": "true"
                    },
                    {
                        "header": "Header 2"
                    },
                    {
                        "title": "Again...a link.",
                        "href": "errrr"
                    },
                    {
                        "title": "Nest Parent",
                        "submenu": [
                            {
                                "title": "nested again",
                                "href": "nested/again"
                            },
                            {
                                "title": "me too",
                                "href": "sample/place"
                            }
                        ]
                    }
                ]
            }
        ];
    })
    .directive('menu', function () {
        return {
            restrict: 'A',
            scope: {
                menu: '=menu',
                cls: '=ngClass'
            },
            replace: true,
            template: '<ul><li ng-repeat="item in menu" menu-item="item"></li></ul>',
            link: function (scope, element, attrs) {
                element.addClass(attrs.class);
                element.addClass(scope.cls);
            }
        };
    })


;


