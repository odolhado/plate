angular.module('ngBoilerplate.dash', [])

    .config(function config($stateProvider) {
        $stateProvider.state('ui.dash', {
            parent: 'ui',
            url: '/dash',
            views: {
                "main@root": {
                    templateUrl: 'ui/dash/dash.tpl.html'
                },
                "footer@root": {
                    templateUrl: 'footer/footer.alt.tpl.html'
                }
            },
            data: {pageTitle: 'dashboard'},
            ncyBreadcrumb: {
                label: 'Dashboard'
            }
        });
    })


;


