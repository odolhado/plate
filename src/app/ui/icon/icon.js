/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module('ngBoilerplate.icon', ['ui.router'])

    .config(function config($stateProvider) {
        $stateProvider.state('ui.icon', {
            parent: 'ui',
            url: '/icon',
            //views: {
            //    "main": {
            //        controller: 'IconCtrl',
            //        templateUrl: 'ui/icon/icon.tpl.html'
            //    }
            //},
            controller: 'IconCtrl',
            templateUrl: 'ui/icon/icon.tpl.html',
            data: {pageTitle: 'icons'},
            ncyBreadcrumb: {
                label: 'Icons'
            }
        });
    })

// CONTROLLERS

    .controller('IconCtrl', function IconCtrl($scope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];
    })




;

