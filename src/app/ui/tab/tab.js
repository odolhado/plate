angular.module('ngBoilerplate.tab', [
    //'ngBoilerplate.tab.contacts',
    //'ngBoilerplate.tab.contacts.service',
    //'ngBoilerplate.utils.service',
    'ui.router'
    //'ngAnimate'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('ui.tab', {
                parent: 'ui',
                url: '/tab',
                //views: {
                //    "main": {
                        controller: 'TabCtrl',
                        templateUrl: 'ui/tab/tab.tpl.html',
                //    }
                //},
                data: {pageTitle: 'tabs'},
                ncyBreadcrumb: {
                    label: 'Tabs'
                }
            });
    })

    //.controller('TabCtrl', function PrefCtrl($scope) {
    //
    //})

    .controller('TabCtrl', function ($scope, $window) {
        $scope.tabs = [
            {title: 'Dynamic Title 1', content: 'Dynamic content 1'},
            {title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true}
        ];

        $scope.alertMe = function () {
            setTimeout(function () {
                $window.alert('You\'ve selected the alert tab!');
            });
        };
    })

    .controller('Tab2Ctrl', function($scope){
        var tabClasses;

        function initTabs() {
            tabClasses = ["","","",""];
        }

        $scope.getTabClass = function (tabNum) {
            return tabClasses[tabNum];
        };

        $scope.getTabPaneClass = function (tabNum) {
            return "tab-pane " + tabClasses[tabNum];
        };

        $scope.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        $scope.tab1 = "This is first section";
        $scope.tab2 = "This is SECOND section";
        $scope.tab3 = "This is THIRD section";
        $scope.tab4 = "This is FOUTRH section";

        //Initialize
        initTabs();
        $scope.setActiveTab(1);
    })

;