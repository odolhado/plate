angular.module('ngBoilerplate.table', [])

    .config(function config($stateProvider) {
        $stateProvider.state('ui.table', {
            parent: 'ui',
            url: '/table',
            templateUrl: 'ui/table/table.tpl.html',
            controller: 'TableCtrl',
            data: {pageTitle: 'table'},
            ncyBreadcrumb: {
                label: 'Table'
            }
        });
    })

    .controller('TableCtrl', function ($scope) {

    })


;


