angular.module('ngBoilerplate.toggle', [
    'ipCookie',
    'ui.router'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('ui.toggle', {
                parent: 'ui',
                url: '/toggle',
                controller: 'ToggleCtrl',
                templateUrl: 'ui/toggle/toggle.tpl.html',
                data: {pageTitle: 'toggle'},
                ncyBreadcrumb: {
                    label: 'Toggle'
                }
            });
    })

    .controller('ToggleCtrl', function($scope, $window) {
        $scope.toggle = function () {
            $scope.isVisible = !$scope.isVisible;
        };
        $scope.isVisible = true;

    })

    .controller("BtnCtrl", function ($scope) {
        $scope.class = "alert alert-danger";
        $scope.name = "red";
        $scope.changeClass = function () {
            if ($scope.class === "alert alert-danger") {
                $scope.class = "alert alert-info";
                $scope.name = "blue";
            }
            else {
                $scope.class = "alert alert-danger";
                $scope.name = "red";
            }
        };
    })

;