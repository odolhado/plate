angular.module('ngBoilerplate.ui', [
    'plusOne',
    'ipCookie',
    'ngBoilerplate.icon',
    'ngBoilerplate.tab',
    'ngBoilerplate.dash',
    'ngBoilerplate.pagination',
    'ngBoilerplate.timepicker',
    'ngBoilerplate.table',
    'ngBoilerplate.button',
    'ngBoilerplate.datepicker',
    'ngBoilerplate.toggle',
    'ngBoilerplate.tooltip'
])

    .config(function config($stateProvider) {
        $stateProvider.state('ui', {
            parent: 'root',
            url: '/ui',
            abstract: true,
            views: {
                "main": {
                    controller: 'UiCtrl',
                    templateUrl: 'ui/ui.tpl.html'
                },
                "footer@root": {
                    template: '<br><br><br>'
                }
            },
            data: {pageTitle: 'ui'},
            ncyBreadcrumb: {
                label: 'User Interface'
            }
        });
    })

    .controller('UiCtrl', function UiController($scope) {
    })

    .controller("SidebarCtrl", function ($scope, $log, ipCookie) {

        $scope.name = "c";

        if ( ipCookie('isSidebarExpanded')===false) {
            $scope.sidebarClass = "sidebar sidebar-collapsed";
            $scope.mainClass = "main-sidebar-collapsed";
            $scope.arrowDirectionClass = "fa fa-angle-right";
        } else {
            $scope.sidebarClass = "sidebar sidebar-expanded";
            $scope.mainClass = "main-sidebar-expanded";
            $scope.arrowDirectionClass = "fa fa-angle-left";
        }
        // this cookie will expire in 30 days, if not reset
        var optionExpire = { expires: 30 };

        $scope.changeClass = function () {
            if ($scope.sidebarClass === "sidebar sidebar-expanded") {
                $scope.sidebarClass = "sidebar sidebar-collapsed";
                $scope.mainClass = "main-sidebar-collapsed";
                //$scope.name = "c";
                $scope.arrowDirectionClass = "fa fa-angle-right";
                ipCookie('isSidebarExpanded', false, optionExpire);
            } else {
                $scope.sidebarClass = "sidebar sidebar-expanded";
                $scope.mainClass = "main-sidebar-expanded";
                //$scope.name = "e";
                $scope.arrowDirectionClass = "fa fa-angle-left";
                ipCookie('isSidebarExpanded', true, optionExpire);
            }
        };
    })

;

