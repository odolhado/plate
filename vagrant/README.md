Install vagrant (https://www.vagrantup.com/downloads.html) and plugin:

```bash
vagrant plugin install vagrant-hostsupdater
```

Install VirtualBox (https://www.virtualbox.org/wiki/Downloads)

Install ansible-playbook:

```bash 
brew install ansible
```

Use it

```bash
cd $PROJECT_ROOT/vagrant
vagrant up
vagrant ssh
cd /srv/www/
./app/console oro:install --timeout=5000 --force # Takes about 30 minutes (excluding questions in the middle. Default Application URL is: oro.example.com)
```

PHPStorm -> Preferences -> Tools -> Vagrant -> Instance folder -> $PROJECT_ROOT/vagrant

Optionally run on host (to avoid asking for a password):
```bash
sudo chmod o+w /etc/hosts
```